using System;
using UnityEngine;

public abstract class Shape : MonoBehaviour, UIMainScene.IUIInfoContent // INHERITANCE
{
    protected string _color;

    public string Color => _color; // ENCAPSULATION

    protected void Start() // INHERITANCE
    {
        _color = GetComponent<MeshRenderer>().material.GetColor("_Color").ToString();
    }

    public virtual string GetName() // POLYMORPHISM
    {
        return nameof(Shape);
    }

    public virtual string GetData() // POLYMORPHISM
    {
        return String.Empty;
    }

    public string GetColor() // ABSTRACTION
    {
        return $"Color is {Color}";
    }
}