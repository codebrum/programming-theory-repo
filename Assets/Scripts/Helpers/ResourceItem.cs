using UnityEngine;

[CreateAssetMenu(fileName = "ResourceItem", menuName = "Ptp/Resource Item")]
public class ResourceItem : ScriptableObject
{
    public string Name;
    public string Id;
}