using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ResourcesDatabase", menuName = "Ptp/Resources Database")]
public class ResourceDatabase : ScriptableObject
{
    public List<ResourceItem> ResourceTypes = new List<ResourceItem>();

    private Dictionary<string, ResourceItem> _database;

    public void Init()
    {
        _database = new Dictionary<string, ResourceItem>();
        foreach (var resourceItem in ResourceTypes)
        {
            _database.Add(resourceItem.Id, resourceItem);
        }
    }

    public ResourceItem GetItem(string uniqueId)
    {
        _database.TryGetValue(uniqueId, out ResourceItem type);
        return type;
    }
}