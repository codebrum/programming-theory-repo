using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScene : MonoBehaviour
{
    public void MoveNextScene()
    {
        SceneManager.LoadScene(1);
    }
}