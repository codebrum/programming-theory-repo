public class Capsule : Shape
{
    public override string GetName()
    {
        return nameof(Capsule);
    }

    public override string GetData()
    {
        return "This is a Capsule.\n" + GetColor();
    }
}