public class Cube : Shape
{
    public override string GetName()
    {
        return nameof(Cube);
    }

    public override string GetData()
    {
        return "This is a Cube.\n" + GetColor();
    }
}