public class Plane : Shape
{
    public override string GetName()
    {
        return nameof(Plane);
    }

    public override string GetData()
    {
        return "This is a Plane.\n" + GetColor();
    }
}