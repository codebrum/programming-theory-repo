public class Quad : Shape
{
    public override string GetName()
    {
        return "Shape";
    }

    public override string GetData()
    {
        return "This is a Quad, but let's just assume it's called a \"Shape\" class(inheritance) for the purposes of this test.\n" + GetColor();
    }
}