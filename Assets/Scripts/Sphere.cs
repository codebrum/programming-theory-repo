public class Sphere : Shape
{
    public override string GetName()
    {
        return nameof(Sphere);
    }

    public override string GetData()
    {
        return "This is a Sphere.\n" + GetColor();
    }
}