public class Cylinder : Shape
{
    public override string GetName()
    {
        return nameof(Cylinder);
    }

    public override string GetData()
    {
        return "This is a Cylinder.\n" + GetColor();
    }       
}