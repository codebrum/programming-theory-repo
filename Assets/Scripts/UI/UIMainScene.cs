using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMainScene : MonoBehaviour
{
    public static UIMainScene Instance { get; private set; }

    public interface IUIInfoContent
    {
        string GetName();
        string GetData();
        // void GetContent(ref List<Shape.InventoryEntry> content);
    }

    public InfoPopup InfoPopup;
    public ResourceDatabase ResourceDB;

    protected IUIInfoContent _currentContent;
    // protected List<Shape.InventoryEntry> _contentBuffer = new();

    private void Awake()
    {
        Instance = this;
        InfoPopup.gameObject.SetActive(false);
        ResourceDB.Init();
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    private void Update()
    {
        if (_currentContent == null)
            return;

        //This is not the most efficient, as we reconstruct everything every time. A more efficient way would check if
        //there was some change since last time (could be made through a IsDirty function in the interface) or smarter
        //update (match an entry content ta type and just update the count) but simplicity in this tutorial we do that
        //every time, this won't be a bottleneck here. 

        InfoPopup.Data.text = _currentContent.GetData();

        InfoPopup.ClearContent();
        // _contentBuffer.Clear();

        // _currentContent.GetContent(ref _contentBuffer);
        // foreach (var entry in _contentBuffer)
        // {
        //     InfoPopup.AddToContent(entry.ShapeName);
        // }
    }

    public void SetNewInfoContent(IUIInfoContent content)
    {
        if (content == null)
        {
            InfoPopup.gameObject.SetActive(false);
        }
        else
        {
            InfoPopup.gameObject.SetActive(true);
            _currentContent = content;
            InfoPopup.Name.text = content.GetName();
        }
    }

    public void BackToTheMenuScene()
    {
        SceneManager.LoadScene(0);
    }
}