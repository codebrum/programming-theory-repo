using UnityEngine;
using UnityEngine.UI;

public class InfoPopup : MonoBehaviour
{
    public Text Name;
    public Text Data;
    public RectTransform ContentTransform;

    public ContentEntry EntryPrefab;

    public void ClearContent()
    {
        foreach (Transform child in ContentTransform)
        {
            Destroy(child.gameObject);
        }
    }
    
    public void AddToContent(string name)
    {
        var newEntry = Instantiate(EntryPrefab, ContentTransform);

        newEntry.ShapeName.text = name.ToString();
    }
}
