using UnityEngine;

public class UserControl : MonoBehaviour
{
    public Camera GameCamera;
    public GameObject Marker;

    private Shape _selected = null;

    private void Start()
    {
        Marker.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            HandleSelection();
        }

        MarkerHandling();
    }

    // manage everything that occurs when the user left-clicks one of the Forklifts in the application
    public void HandleSelection()
    {
        var ray = GameCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            //the collider could be children of the unit, so we make sure to check in the parent
            var unit = hit.collider.GetComponentInParent<Shape>();
            _selected = unit;


            //check if the hit object have a IUIInfoContent to display in the UI
            //if there is none, this will be null, so this will hid the panel if it was displayed
            var uiInfo = hit.collider.GetComponentInParent<UIMainScene.IUIInfoContent>();
            UIMainScene.Instance.SetNewInfoContent(uiInfo);
        }
    }

    // Handle displaying the marker above the unit that is currently selected (or hiding it if no unit is selected)
    void MarkerHandling()
    {
        if (_selected == null && Marker.activeInHierarchy)
        {
            Marker.SetActive(false);
            Marker.transform.SetParent(null);
        }
        else if (_selected != null && Marker.transform.parent != _selected.transform)
        {
            Marker.SetActive(true);
            Marker.transform.SetParent(_selected.transform, false);
            Marker.transform.localPosition = Vector3.zero - new Vector3(transform.position.x, 1);
        }
    }
}